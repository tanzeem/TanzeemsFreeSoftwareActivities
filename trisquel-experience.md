### `Trisquel GNU/Linux 7.0` Installation experience on HP Laptop

`Trisquel GNU/Linux` is the most popular among the `Fully Free GNU/Linux Operating systems`.
certified by the Free Software Foundation Guidelines
Similar others are `gnewSense`, `Parabola`, `Dragora` and `GUIX`.
`Debian GNU/Linux` is also among the same category if the `main repo only is enabled`.
Other most common distros like Mint, Ubuntu GNU/Linux, CentOS, Gentoo, Arch , Mandriva etc
falls a little back in ensuring freedom to the user, which actually should be the
main motive as GNU suggests. Especially the Ubuntu since it supports lavish non-free 
softwares and  it has a privacy feature disabled by default
 causing its users to be easily spied by Canonical which owns Ubuntu.
 The Ubuntu's Activity log is installed by default and most
 users dont know that it keeps track of the
 usage statistics and send to Canonical server.
 Then it was Trisquel that came to the rescue of ensuring the freedom of 
 computer users by intelligently filtering all the non-free packages from 
Ubuntu repository. The filtering diagram is available in Trisquel web site.

### Success

- Installation completed successfully from my `live USB`.
- Could connect to internet by USB tethering with smartphone.
- All is well when firmware matches the GNU rules

#### Fallbacks
---
- But since my hardware got non-free firmware, my audio doesnt work.
- Booted my Trisquel from usb
- Direct installation from usb/disk  failed  
  at the point of mirror selection
- The installation of Trisquel mostly fails at the mirror selection step.
- The installation after booting into Trisquel Live session only was successful
- Also the installation could be completed only without net connection
and in the install from live mode also, no mirrors were detected.


 Earlier I have tried the same using Trisquel live CD also and had the same experience.

 Written by 
> `Mr. Tanzeem M B`
 working for `C-DIT` in FOSS technologies since 2009

[Tags followed] 

C-DIT, SPACE, ICFOSS, Govt of Kerala, FSUG-TVM, FSF India, Govt of India

Technoethical, Fully-free GNU/Linux distros, H-Node, FSF, GNU, EFF, DFF


